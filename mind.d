// humind.d
import arsd.terminal;
import std.exception;
import std.stdio;


class Language 
{	
	enum Type
	{	
		Syntax,    // is syntax or contains a single syntax type
		Container  // contains language
	}

	const   Type language_type;
	const string text;

	this (in Type type, in string text,) 
	{
		this.language_type= type; //(text == null)? Type.Container : Type.Syntax;
		this.text         = text;
	}

	override  // Object
	string 
	toString () const 
	{
		return text is null? "" : text;
	}

	string 
	classname () const
	{
		import std.array: split;
		import std.uni: toLower;

		auto           fullclassname= typeid(this).name;
		auto fullclassnameitems= split(fullclassname, ".");
		auto classname= fullclassnameitems[$ - 1].toLower;

		return classname;
	}

	string 
	tagname () const
	{
		return classname;
	}

	string 
	otag () const
	{
		return ("<" ~ tagname ~ ">");
	}

	string 
	ctag () const
	{
		return ("</" ~ tagname ~ ">");
	}


	unittest
	{
		stdout.writeln("Language class test 1 (syntax)");

		auto language= new Language(Language.Type.Syntax, "syntax");

		assert (language.language_type == Language.Type.Syntax);
		assert (language.text          == "syntax");
		assert (language.toString      == "syntax");
		assert (language.otag          == "<language>");
		assert (language.ctag          == "</language>");
	}

	unittest
	{
		stdout.writeln("Language class test 2 (container)");

		auto language= new Language(Language.Type.Container, null);

		assert (language.language_type == Language.Type.Container);
		assert (language.text          == null);
		assert (language.toString      == "");
		assert (language.otag          == "<language>");
		assert (language.ctag          == "</language>");
	}
} // Language


// --- Syntax :Language ---
class Syntax :Language
{	// single grammatical element
	// instantiate only in tests

	enum Type
 	{
		none,        // inside a syntax·container
		Determiner,
		Noun,
		Pronoun,
		Adjective,
		Verb,
		ActiveVerb,
		CompleteVerb,
		PassiveVerb,
		LinkingVerb,
		HelpingVerb,
		Adverb,
		Preposition,
		Conjunction,
		Blankspace,
		Linkspace,
		Punctuation
	}

	static const linkspace = "·";
	static const blankspace= " ";

	const    string text_separator; 
	const      Type syntax_type;
	const  Syntax[] items;
	const Container subsyntax;

	this (	in Syntax[] items,
			in     Type syntax_type,
		  	in   string text_separator= Syntax.blankspace) 
	{
		enforce (items  != null);
		enforce (syntax_type != Type.none);

		super (Language.Type.Syntax, null);
		
		this.text_separator= text_separator;
		this.syntax_type       = syntax_type;
		this.items             = items;
		this.subsyntax         = null;
	}

	this (	in Syntax item,
			in   Type syntax_type) 
	{
		this ([item], syntax_type);
	}

	this (	in string text,
			in Type syntax_type= Type.none)
	{
		enforce   (text != null);

		super (Language.Type.Syntax, text);

		this.text_separator= null;
		this.syntax_type   = syntax_type;
		this.items         = null;
		this.subsyntax     = null;
	}

	this (	in Container       subsyntax,
			in Type          syntax_type= Type.none,
	  	  	in string text_separator= blankspace)
	{
		super (Language.Type.Syntax, null);

		this.text_separator= text_separator;
		this.syntax_type   = syntax_type;
		this.subsyntax     = subsyntax;
		this.items         = null;

	}

	override // language
	string toString() const 
	{
		if (!(this.text is null))
		{
			return this.text;
		}
		else if (!(subsyntax is null))
		{
			return subsyntax.toString;
		}
		else
		{
			assert ((items != null));

			string text = "";

			if (text_separator is null)
			{
				foreach (ref item; items) 
				{
					text~= item.toString;
				}
			}
			else
			{
				auto items_length= items.length;
				if (items_length > 0)
				{
					auto last_index= items_length - 1;
					text~= items[0].toString;
					int index= 1;
					while (index <= last_index)
					{
						auto item= items[index];
						auto punctuation_item= (			item.language_type == Language.Type.Syntax &&
												(cast(Syntax)item).syntax_type == Syntax.Type.Punctuation);
						text~= (punctuation_item? item.toString : text_separator ~ item.toString);
						index++;
					}
				}
			}

			return text;
		}
	}

	unittest
	{
		stdout.writeln("Syntax class test 1 (syntax_text, syntax_type)");

		auto syntax= new Syntax("verb", Syntax.Type.ActiveVerb);

		assert (syntax.language_type == Language.Type.Syntax);
		assert (syntax.syntax_type   == Syntax.Type.ActiveVerb);
		assert (syntax.text          == "verb");
		assert (syntax.toString      == "verb");
		assert (syntax.otag          == "<syntax>");
		assert (syntax.ctag          == "</syntax>");
	}

	unittest
	{
		stdout.writeln("Syntax class test 2 (syntax_text)");

		auto syntax= new Syntax("verb");

		assert (syntax.language_type == Language.Type.Syntax);
		assert (syntax.syntax_type   == Syntax.Type.none);
		assert (syntax.text          == "verb");
		assert (syntax.toString      == "verb");
		assert (syntax.otag          == "<syntax>");
		assert (syntax.ctag          == "</syntax>");
	}

	unittest
	{
		stdout.writeln("Syntax class test 3 (syntax_items, syntax_type)");

		auto container= new Syntax
							( 
							 	[	new Syntax ("was"),
							 	 	new Syntax ("kicked")
							 	],
 								Syntax.Type.ActiveVerb
							);

		assert (container.language_type == Language.Type.Syntax);
		assert (container.syntax_type        == Syntax.Type.ActiveVerb);
		assert (container.text          == null);
		assert (container.toString      == "was kicked");
		assert (container.otag          == "<syntax>");
		assert (container.ctag          == "</syntax>");

		foreach (ref item; container.items)
		{
			assert (item.syntax_type == Syntax.Type.none);
		}
	}

	unittest
	{
		stdout.writeln("Syntax class test 4 (syntax_item)");

		auto syntax= new Syntax (new Syntax ("kicked"),
									Syntax.Type.ActiveVerb);

		assert (syntax.language_type       == Language.Type.Syntax);
		assert (syntax.syntax_type         == Syntax.Type.ActiveVerb);
		assert (syntax.text                == null);
		assert (syntax.toString            == "kicked");
		assert (syntax.items.length        == 1);
		assert (syntax.items[0].syntax_type == Syntax.Type.none);
	}
} // Syntax

class Pronoun :Syntax
{
	enum Type
	{
		Demonstrative,
		Indefinite,
		Interrogative,
		Personal,
		Possessive,
		Relative,
		Reciprocal,
		Reflexive
	}

	immutable Type pronoun_type;

	this (	in     Type pronoun_type,
			in Syntax[] items) 
	{
		super (items, Syntax.Type.Pronoun);
		this.pronoun_type= pronoun_type;
	}

	this (	in   Type pronoun_type, 
			in string text) 
	{
		super (text, Syntax.Type.Pronoun);
		this.pronoun_type= pronoun_type;
	}

	unittest
	{
		{
			stdout.writeln("Pronoun class test 1 (pronoun·type·personal, pronoun_text)");

			auto pronoun= new Pronoun
								(   Pronoun.Type.Personal,
								 	[new Syntax ("you")],
								);

			assert (pronoun.language_type == Language.Type.Syntax);
			assert (pronoun.syntax_type   == Syntax.Type.Pronoun);
			assert (pronoun.pronoun_type  == Pronoun.Type.Personal);
			assert (pronoun.text          == "");
			assert (pronoun.toString      == "you");
			assert (pronoun.otag          == "<pronoun>");
			assert (pronoun.ctag          == "</pronoun>");

			auto syntax= pronoun.items[0];
			assert (syntax.syntax_type == Syntax.Type.none);
			assert (syntax.text == "you");
		}
	}

	unittest
	{
		stdout.writeln("Pronoun class test 2 (pronoun·type·indefinite, pronoun_text)");

		auto pronoun= new Pronoun
							(	Pronoun.Type.Indefinite, 
								"all");

		assert (pronoun.language_type == Language.Type.Syntax);
		assert (pronoun.syntax_type   == Syntax.Type.Pronoun);
		assert (pronoun.pronoun_type  == Pronoun.Type.Indefinite);
		assert (pronoun.text          == "all");
		assert (pronoun.toString      == "all");
		assert (pronoun.otag          == "<pronoun>");
		assert (pronoun.ctag          == "</pronoun>");
		assert (pronoun.items         is null);
	}
}

class RelativePronoun :Pronoun 
{	// relative·pronouns: which, that, who, whom, whose
	// . used to head a relative·clause (adjective·clause), which adds more information about a noun

	enum ID
	{
		WHICH, THAT, WHO, WHOM, WHOSE
	}

	static string[ID] ids;
	static string[]   values;

	@disable // Pronoun
		this (in Type pronoun_type, in Syntax[] items);

	this (in ID relpron_id)
	{
		//super (value_for(relpron_id));
		super (Pronoun.Type.Relative, ids[relpron_id]);
	}

	this (in string syntax_text)
	{
		import std.string: strip;
		import std.uni   : toLower;
		import std.algorithm: canFind;

		if (!values.canFind(syntax_text))
		{
			throw new Exception ("invalid relative·pronoun");
		}

		super (Pronoun.Type.Relative, syntax_text);
	}

	static string value_for(ID id)
	{
		string syntax_text;

		final switch 
		(id)
		{
			case ID.WHICH:
				syntax_text= "which";
				break;

			case ID.THAT:
			syntax_text= "that";
				break;

			case ID.WHO:
			syntax_text= "who";
				break;

			case ID.WHOM:
			syntax_text= "whom";
				break;

			case ID.WHOSE:
				syntax_text= "whose";
				break;
		}
		return syntax_text;
	}
	
	static this()
	{
		ids= 
			[	ID.WHICH : "which",
				ID.THAT  : "that",
				ID.WHO   : "who",
				ID.WHOM  : "whom",
				ID.WHOSE : "whose"
			];
		values= ids.values;
	}


	unittest
	{
		stdout.writeln("RelativePronoun class test 1 (pronoun_text)");

		auto rel_pronoun= new RelativePronoun ("which");

		assert (rel_pronoun.pronoun_type == Pronoun.Type.Relative);
		assert (rel_pronoun.text         == "which");
		assert (rel_pronoun.tagname      == "relativepronoun");
		assert (rel_pronoun.items        == null);
	}

	unittest
	{
		stdout.writeln("RelativePronoun class test 2 (pronoun_id)");

		auto rel_pronoun= new RelativePronoun (RelativePronoun.ID.THAT);

		assert (rel_pronoun.pronoun_type == Pronoun.Type.Relative);
		assert (rel_pronoun.text         == "that");
		assert (rel_pronoun.tagname      == "relativepronoun");
		assert (rel_pronoun.items        == null);
	}

	unittest
	{
		stdout.writeln("RelativePronoun class test 3 (throw for invalid relative·pronoun)");

		assertThrown!Exception (new RelativePronoun ("where"));	
		
	}

}

class PersonalPronoun :Pronoun
{	// a short word used as a substitute for a proper·noun
	// . these words are: i, uou, she, zie, he, her, zir, him, it, we, they, who

	@disable // Pronoun
		this (in Type pronoun_type, in Syntax[] items);

	this (in string pronoun_text)
	{
		super (Pronoun.Type.Personal, pronoun_text);
	}

	unittest
	{
		stdout.writeln("PersonalPronoun class test (pronoun_text)");

		auto rel_pronoun= new PersonalPronoun ("zie");

		assert (rel_pronoun.pronoun_type == Pronoun.Type.Personal);
		assert (rel_pronoun.text         == "zie");
		assert (rel_pronoun.tagname      == "personalpronoun");
		assert (rel_pronoun.items        == null);
	}
}


class Adjective :Syntax
{	// syntax that describes nouns or pronouns

	//const Syntax referent;

	this (in string text)
	{
		//enforce (referent is null || valid (referent));

		super (text, Syntax.Type.Adjective);
		//this.referent= referent;
	}

	this (in Syntax[] items) 
	{	// TODO: change to Component (with possible ComparativeAdjective, SuperlativeAdjective subclasses)
		//enforce (referent is null || valid (referent));
		super (items, Syntax.Type.Adjective);
		//this.referent= referent;
	}

	//private bool 
	//valid (in Syntax referent)
	//{
	//	return referent.syntax_type == Syntax.Type.Noun || referent.syntax_type == Syntax.Type.Pronoun;
	//} 

	unittest
	{
		stdout.writeln("Adjective class test 1: syntax, referent");

		auto syntax= new Adjective
							( 
							 	[ new Adjective ("bright"),
							 	  new Adjective ("red")
							 	]//,
							 	//new Noun (Noun.Type.Common, "bicycle")
							);

		assert (syntax.language_type == Language.Type.Syntax);
		assert (syntax.syntax_type        == Syntax.Type.Adjective);
		assert (syntax.text          == "");
		assert (syntax.toString      == "bright red");
		assert (syntax.otag          == "<adjective>");
		assert (syntax.ctag          == "</adjective>");
		//assert (syntax.referent.text == "bicycle");
		assert (syntax.items[0].text == "bright");
		assert (syntax.items[1].ctag == "</adjective>");
	}

	unittest
	{
		stdout.writeln("Adjective class test 2: text, referent");

		auto syntax= new Adjective
							( "red"//, 
							   //new Noun (Noun.Type.Common, "bicycle")
							);

		assert (syntax.language_type   == Language.Type.Syntax);
		assert (syntax.syntax_type          == Syntax.Type.Adjective);
		assert (syntax.text            == "red");
		assert (syntax.toString        == "red");
		assert (syntax.otag            == "<adjective>");
		assert (syntax.ctag            == "</adjective>");
		//assert (syntax.referent.type == Syntax.Type.Noun);

		foreach (ref item; syntax.items)
		{
			assert (item.syntax_type == Syntax.Type.none);
		}
	}
}

class Participle :Adjective 
{
	this (in string text)
	{
		super (text);
	}

	this (in Syntax[] items)
	{
		super (items);
	}

	this (in Syntax item)
	{
		this ([item]);
	}
}


class Adverb :Syntax
{	// syntax that describes verbs, adjectives, adverbs

	this (in string text)
	{
		//enforce (referent is null || valid (referent));

		super (text, Syntax.Type.Adverb);
		//this.referent= referent;
	}

	this (in Syntax[] items) 
	{	// TODO: change to Component (with possible ComparativeAdjective, SuperlativeAdjective subclasses)
		//enforce (referent is null || valid (referent));
		super (items, Syntax.Type.Adverb);
		//this.referent= referent;
	}
}

class Preposition :Syntax
{	// syntax that describes verbs

	this (in string text)
	{
		super (text, Syntax.Type.Preposition);
	}

	this (in Syntax[] items) 
	{	
		super (items, Syntax.Type.Preposition);
	}
}

class Punctuation :Syntax
{	
	enum Type 
		{	comma,    period,      colon,             semicolon,
			question, exclamation, single_quot,       double_quot,
			open_single_quot,      close_single_quot,
			open_double_quot,      close_double_quot,
			open_paren,            close_paren, 
			open_bracket,          close_bracket,
			open_brace,            close_brace
	}
	immutable Type type;

	@disable // Syntax
		this (in Type type, in string text);

	this (in Type type)
	{	import std.conv: to;
		auto letter= ",.:;?!\'\"‘’“”()[]{}"[type];
		super (letter.to!string, Syntax.Type.Punctuation);
		this.type= type;
	}

	unittest
	{
		stdout.writeln("Punctuation class test (Sytax.Type.Punctuation)");

		auto punc= new Punctuation (Punctuation.Type.comma);

		assert (punc.text == ",");
	}
}

class Determiner :Syntax
{
	enum Type
	{
		Definite,
		Indefinite,
		Demonstrative,
		Possessive,  // my your zir her his its our their
		Number,
		Distributive,
		Interrogative,
		Quantifier,
		Difference
	}

	immutable Type type;

	//disable Syntax constructors
	@disable
		this (in string text, in Type type);
	@disable
		this (in string text);
	@disable
		this (in Syntax[] items, in Syntax.Type type);

	this (in Type type, in string text= "")
	{
		super (type == Type.Definite? "the" : text, Syntax.Type.Determiner);

		this.type= type;
	}

	unittest
	{
		stdout.writeln("Determiner class test 1 (definite)");

		auto det= new Determiner (Determiner.Type.Definite);
		assert (det.language_type == Language.Type.Syntax);
		assert (det.syntax_type        == Syntax.Type.Determiner);
		assert (det.text          == "the");
		assert (det.toString      == "the");
		assert (det.otag          == "<determiner>");
		assert (det.ctag          == "</determiner>");
	}

	unittest
	{
		stdout.writeln("Determiner class test 2 (Possesive, \"zir\")");

		auto det= new Determiner (Determiner.Type.Possessive, "zir");
		assert (det.language_type == Language.Type.Syntax);
		assert (det.syntax_type        == Syntax.Type.Determiner);
		assert (det.text          == "zir");
		assert (det.otag          == "<determiner>");
		assert (det.ctag          == "</determiner>");
	}
}

class Noun :Syntax
{
	enum Type
	{
		Common,
		Proper,
		Abstract,
		Countable,
		Uncountable,
		Compound,
		Collective,
		Singular,
		Plural,
		Possessive,
		Gerund
	}

	/*enum Job
	{
		Subject,
		DirectObject,
		IndirectObject,
		Prepositional,
		PredicateNoun,
		ObjectComplement,
		Appositive,
		DirectAddress
	}*/

	immutable Type noun_type;
	//immutable Job  noun_job;

	this (in Type type, string text) 
	{
		super (text, Syntax.Type.Noun);

		this.noun_type= type;
		//this.noun_job = job;
	}

	this (	in Type type,
			in Syntax item) 
	{
		this (type, [item]);
	}

	this (in Type type, in Syntax[] items) 
	{
		super (items, Syntax.Type.Noun);

		this.noun_type= type;
		//this.noun_job = job;
	}

	unittest
	{
		stdout.writeln("Noun class test 1: compound noun");
		{
			auto noun= new Noun
								(	Noun.Type.Common,
								 	[new Syntax ("helicopter"),
								 	 new Syntax ("pad")
								 	]
								);

			assert (noun.language_type == Language.Type.Syntax);
			assert (noun.noun_type     == Noun.Type.Common);
			//assert (noun.noun_job      == Noun.Job.Subject);
			assert (noun.syntax_type        == Syntax.Type.Noun);
			assert (noun.text          == "");
			assert (noun.toString      == "helicopter pad");
			assert (noun.otag          == "<noun>");
			assert (noun.ctag          == "</noun>");

			foreach (ref item; noun.items)
			{
				assert (item.syntax_type == Syntax.Type.none);
			}
		}
	}

	unittest
	{
		stdout.writeln("Noun class test 2: simple noun");
		{
			auto noun= new Noun(Noun.Type.Common,
								"helicopter");

			assert (noun.language_type == Language.Type.Syntax);
			assert (noun.syntax_type        == Syntax.Type.Noun);
			assert (noun.noun_type     == Noun.Type.Common);
			//assert (noun.noun_job      == Noun.Job.Subject);
			assert (noun.text          == "helicopter");
			assert (noun.toString      == "helicopter");
			assert (noun.otag          == "<noun>");
			assert (noun.ctag          == "</noun>");
			assert (noun.items         == null);
		}
	}

	unittest
	{
		stdout.writeln("Noun class test 3: gerund");
		{
			auto clause=
			new Clause
				([	new Gerund (new ActiveVerb ("running")),
					//([ new ActiveVerb ("running")
					//]),
					new CommonNoun ("marathons"),
					new ActiveVerb ("is"),
					new Adjective  ("fun")
				]);

			assert (clause.language_type == Language.Type.Container);
			auto item0_as_lang= clause.items[0];
			assert (item0_as_lang.language_type == Language.Type.Syntax);
			auto item0_as_syn= cast(Syntax)item0_as_lang;
			assert (item0_as_syn.syntax_type == Syntax.Type.Noun);
			auto item0_as_noun= cast(Noun)item0_as_syn;
			assert (item0_as_noun.noun_type == Noun.Type.Gerund);
			auto item0_0_as_syn= item0_as_noun.items[0];
			assert (item0_0_as_syn.syntax_type == Syntax.Type.ActiveVerb);
			assert (item0_0_as_syn.text == "running");
			assert (clause.toString == "running marathons is fun");
		}
	}
}

class ProperNoun :Noun
{
	this (in string noun_text)
	{
		super (Noun.Type.Proper, noun_text);
	}

	this (in Syntax[] noun_items)
	{
		super (Noun.Type.Proper, noun_items);
	}
}

class CommonNoun :Noun
{
	this (in string noun_text)
	{
		super (Noun.Type.Common, noun_text);
	}

	this (in Syntax[] noun_items)
	{
		super (Noun.Type.Common, noun_items);
	}
}

class Gerund :Noun 
{
	this (in string noun_text)
	{
		super (Noun.Type.Gerund, noun_text);
	}

	this (in Syntax[] noun_items)
	{
		super (Noun.Type.Gerund, noun_items);
	}

	this (in Syntax noun_item)
	{
		this ([noun_item]);
	}
}

// --- Separator :Syntax ---
class Separator :Syntax 
{	
	@disable // Syntax
	this (in Type type, in string text);

	this (in Type space_type) 
	{
		enforce (space_type == Type.Blankspace || space_type == Type.Linkspace);

		super ((space_type == Type.Blankspace? blankspace : linkspace), space_type);
	}

	unittest
	{
		stdout.writeln("Separator class test 1: blanspace");

		auto syntax= new Separator (Syntax.Type.Blankspace);

		assert (syntax.syntax_type        == Syntax.Type.Blankspace);
		assert (syntax.language_type == Language.Type.Syntax);
		assert (syntax.text          == " ");
		assert (syntax.toString      == " ");
		assert (syntax.otag          == "<separator>");
		assert (syntax.ctag          == "</separator>");
	}

	unittest
	{
		stdout.writeln("Separator class test 2: linkspace");

		auto syntax= new Separator (Syntax.Type.Linkspace);

		assert (syntax.syntax_type        == Syntax.Type.Linkspace);
		assert (syntax.language_type == Language.Type.Syntax);
		assert (syntax.text          == "·");
		assert (syntax.toString      == "·");
		assert (syntax.otag          == "<separator>");
		assert (syntax.ctag          == "</separator>");
	}
//class Blankspace :Separator
//{
//	this ()
//	{
//		super (blankspace);
//	}
//}
}

// --- Conjunction ---
class Conjunction :Syntax
{
	@disable // Soeech
		this(in Syntax.Type type, in string text);

	@disable // Syntax
		this (in Syntax.Type type, in string text); 

	this (	in string text)
	{
		super (text, Syntax.Type.Conjunction);
	}
}

class CoordinatingConjunction :Conjunction 
{	
	enum Type 
	{ FOR= 0, AND, NOR, BECAUSE, OR, YET, SO }
	static string[] types = ["for", "and", "nor", "because", "or", "yet", "so"];
	immutable Type type;

	this (in Type type) {
		super (types[type]);
		this.type= type;
	}
}

class SubordinatingConjunction :Conjunction 
{	// after, althought, as soon as, ...

	this (in string text) {
		super (text);
	}
}

class CorrelativeConjunction :Conjunction 
{	// either, or, neither, nor, ...

	this (in string text) {
		super (text);
	}
}

class AdverbConjunction :Conjunction 
{	// after all, besides, consequently, ...

	this (in string text) {
		super (text);
	}
}

// --- Verb :Syntax ---
class Verb :Syntax
{	// simple·verb
	//
	// compound·verb
	//
	// verb·phrase:
	//   · helping·verb + action·verb
	//   · helping·verb + linking·verb
	//
	// phrasal·verb:
	//   · verb + preposition, adverb, or both
	
	enum Type
	{
		Action,
		Linking,   // state·of·being
		Helping    // helps action·verb or linkking·verb
	}

	const Type verb_type;

	this (	in        Type verb_type,
			in Syntax.Type type,
			in      string verb_text)
	{
		super (verb_text, type);
		this.verb_type= verb_type;
	}

	this (	in        Type verb_type,
			in Syntax.Type type,
			in    Syntax[] verb_items)
	{
		super (verb_items, type);
		this.verb_type= verb_type;
	}

	//this (	in     Verb.Type verb_type,
	//		in Syntax.Type type,
	//		in     Container verb_items)
	//{
	//	super (verb_items, type);
	//	this.verb_type= verb_type;
	//}

	unittest
	{
 		stdout.writeln("Verb class test 1 (verbtype-action, type-action·verb, verb_text)");

 		auto verb= new Verb (Verb.Type.Action, Syntax.Type.ActiveVerb, "read");

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
 		assert (verb.syntax_type        == Syntax.Type.ActiveVerb);
 		assert (verb.items         is null);
 		assert (verb.text          == "read");
 		assert (verb.toString      == "read");
 		assert (verb.otag          == "<verb>");
 		assert (verb.ctag          == "</verb>");
	}

	unittest
	{
		stdout.writeln("Verb class test 2 (helping·verb, verb_item)");

		auto verb= new Verb
							(	Verb.Type.Helping,
								Syntax.Type.HelpingVerb,
							 	[
							 		new HelpingVerb ("will"),
							 	]
							);

		assert (verb.language_type   == Language.Type.Syntax);
		assert (verb.verb_type       == Verb.Type.Helping);
		assert (verb.syntax_type          == Syntax.Type.HelpingVerb);
		assert (verb.text            == null);
		assert (verb.toString        == "will");
		assert (verb.otag            == "<verb>");
		assert (verb.ctag            == "</verb>");
		assert (verb.items[0].text   == "will");
	}
} // Verb

class ActionVerb :Verb
{	// written in active·voice
	// content:
	// · active‚verb, complete·verb, passive·verb, adverb

	enum ActionType
	{
		Active,		// has direct·object
		Complete,	// no direct·object
		Passive		// the subject is the receiver of the action
					// the initiator may be denoted by a prepositional·phrase=
					// verb written in passive·voice
	}
	//const         string text_separator; // TODO: needed?
	immutable ActionType action_type;
	//const      Container subject;

	//this (	in ActionType action_type,
	//		in Syntax.Type type,
	//		in Container verb_items)
	//{
	//	super (Verb.Type.Action, type, verb_items);
	//	this.action_type= action_type;
	//	//this.subject    = subject;
	//	this.text_separator= Syntax.blankspace;
	//}

	this (	in    ActionType action_type, 
			in Syntax.Type type,
			in        string verb_text)
	{
		super (Verb.Type.Action, type, verb_text);
		this.action_type= action_type;
		//this.text_separator= Syntax.blankspace;
	}

	this (	in    ActionType action_type, 
			in Syntax.Type type,
			in      Syntax[] verb_items)
	{
		super (Verb.Type.Action, type, verb_items);
		this.action_type= action_type;
		//this.text_separator= Syntax.blankspace;
	}

	/*override // Syntax
	string toString() const 
	{	// TODO: handle these needed spaces smarter
		auto subject_text= subject.toString;
		auto this_text=    super.toString;

		/*string this_text= "";
		if (this.text is null)
		{
			foreach (ref item; items) 
			{
				this_text~= item.toString;
			}
		}
		else
		{
			this_text= this.text;
		}

		if (subject_text.length > 0 && this_text.length > 0)
		{
			subject_text~= Syntax.blankspace;
		}
		return subject_text ~ text_separator ~ this_text;
	}	*/

	unittest
	{
 		stdout.writeln("ActionVerb class test 1 (active, text)");

 		auto verb= new ActionVerb
 								(	ActionType.Active, 
 									Syntax.Type.ActiveVerb,
 									/*new Container  
 										([  // subject
		 									new Pronoun (	Noun.Type.Singular,
		 													Pronoun.Type.Personal,
		 													"i"
		 											 	)
		 								]),*/
 									"kicked"  // verb
 								);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Active);
 		assert (verb.syntax_type        == Syntax.Type.ActiveVerb);
 		assert (verb.items         is null);
 		assert (verb.text          == "kicked");
 		assert (verb.toString      == "kicked");
 		assert (verb.otag          == "<actionverb>");
 		assert (verb.ctag          == "</actionverb>");
	}

	unittest
	{
 		stdout.writeln("ActionVerb class test 2 (actiontype-passive, type-passive·verb, verb_text)");

 		auto verb= new ActionVerb
 								(	ActionType.Active, 
 									Syntax.Type.CompleteVerb,
 									"kicked"
 								);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionType.Active);
 		assert (verb.syntax_type        == Syntax.Type.CompleteVerb);
 		assert (verb.text          == "kicked");
 		assert (verb.toString      == "kicked");
 		assert (verb.otag          == "<actionverb>");
 		assert (verb.ctag          == "</actionverb>");
	}

	unittest
	{
 		stdout.writeln("ActionVerb class test 3: complete, text");

 		auto verb= new ActionVerb
 								(	ActionType.Complete, 
 									Syntax.Type.CompleteVerb,
 									/*new Container  
										([  // subject
											new Determiner (Determiner.Type.Definite),
		 									new Noun (	Noun.Type.Singular,
		 												"ball"
		 											 )
		 								]),*/
 									"rolled"  // verb
 								);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Complete);
 		assert (verb.syntax_type        == Syntax.Type.CompleteVerb);
 		assert (verb.items         is null);
 		assert (verb.text          == "rolled");
 		assert (verb.toString      == "rolled");
 		assert (verb.otag          == "<actionverb>");
 		assert (verb.ctag          == "</actionverb>");
	}
} // ActionVerb

class ActiveVerb :ActionVerb
{	// content:
	// · active·verb [+ adverb]
	// · helping·verb + active·verb

	this (in string verb_text)
	{
		super (ActionType.Active, Syntax.Type.ActiveVerb, verb_text);
	}

	this (in Syntax[] verb_items)
	{
		super (ActionType.Active, Syntax.Type.ActiveVerb, verb_items);
/* humind.d(937): Error: none of the overloads of `this` are callable using argument types 
									  `(ActionType, Syntax, const(Verb[]))`
humind.d(802):        Candidates are: `humind.ActionVerb.this(in ActionType action_type, in Syntax type, in string verb_text)`
humind.d(811):                        `humind.ActionVerb.this(in ActionType action_type, in Syntax type, in ActionVerb[] verb_items)`

*/
	}

	//this (in Container verb_items)
	//{
	//	super (ActionType.Active, Syntax.Type.ActiveVerb, verb_items);
	//}

	unittest
	{
 		stdout.writeln("ActiveVerb class test 1 (active·verb_text)");

 		auto verb= new ActiveVerb
						(	/*new Container  
							([  // subject
								new Pronoun (Noun.Type.Singular, Pronoun.Type.Personal, "i")
							]),*/
							"ate"
							/*new Container  
							([  // direct·object
								new Determiner (Determiner.Type.Number, "five"),
								new Noun       (Noun.Type.Common,       "pizzas")
							])*/
						);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Active);
 		assert (verb.syntax_type        == Syntax.Type.ActiveVerb);
 		assert (verb.items         is null);
 		assert (verb.text          == "ate");
 		assert (verb.toString      == "ate");
 		assert (verb.otag          == "<activeverb>");
 		assert (verb.ctag          == "</activeverb>");
	}

	unittest
	{
 		stdout.writeln("ActiveVerb class test 2 (active·verb·phrase)");

 		auto verb= new ActiveVerb
						([	new HelpingVerb ("did"),
							new ActiveVerb  ("eat")
						]);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Active);
 		assert (verb.syntax_type        == Syntax.Type.ActiveVerb);
 		assert (verb.toString      == "did eat");
 		assert (verb.otag          == "<activeverb>");
 		assert (verb.ctag          == "</activeverb>");

 		auto first_verb= verb.items[0];
 		assert (first_verb.syntax_type == Syntax.Type.HelpingVerb);

 		auto second_verb= verb.items[1];
 		assert (second_verb.syntax_type == Syntax.Type.ActiveVerb);
	}

	unittest
	{
 		stdout.writeln("ActiveVerb class test 3 (active·verb·phrase)");

 		auto verb= new ActiveVerb
						([	new ActiveVerb ("nail"),
							new Adverb     ("down")
						]);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Active);
 		assert (verb.syntax_type        == Syntax.Type.ActiveVerb);
 		assert (verb.toString      == "nail down");
 		assert (verb.otag          == "<activeverb>");
 		assert (verb.ctag          == "</activeverb>");

 		auto first_item= verb.items[0];
 		assert (first_item.syntax_type == Syntax.Type.ActiveVerb);

 		auto second_item= verb.items[1];
 		assert (second_item.syntax_type == Syntax.Type.Adverb);
	}

	unittest
	{
 		stdout.writeln("ActiveVerb class test 4 (helping·verb + active·verb)");

 		auto verb= new ActiveVerb  // active·verb·phrase
						([	new HelpingVerb ("must have been"),
							new ActiveVerb  ("sleeping")
						]);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Active);
 		assert (verb.syntax_type        == Syntax.Type.ActiveVerb);
 		assert (verb.toString      == "must have been sleeping");
 		assert (verb.otag          == "<activeverb>");
 		assert (verb.ctag          == "</activeverb>");

 		auto verb_item0= verb.items[0];
 		assert (verb_item0.syntax_type   == Syntax.Type.HelpingVerb);
 		assert (verb_item0.toString == "must have been");
 		assert (verb_item0.otag     == "<helpingverb>");

 		auto verb_item1= verb.items[1];
 		assert (verb_item1.syntax_type   == Syntax.Type.ActiveVerb);
 		assert (verb_item1.toString == "sleeping");
 		assert (verb_item1.ctag     == "</activeverb>");

	}

}

class CompleteVerb :ActionVerb
{	// content:
	// · main·verb (active·verb, linking·verb)
	// · helping·verb + main·verb

	this (in string verb_text)
	{
		super (ActionType.Complete, Syntax.Type.CompleteVerb, verb_text);
	}

	this (in Verb[] verb_items)
	{
		super (ActionType.Complete, Syntax.Type.CompleteVerb, verb_items);
	}

	//this (in Container verb_items)
	//{
	//	super (ActionType.Complete, Syntax.Type.CompleteVerb, verb_items);
	//}

	unittest
	{
 		stdout.writeln("CompleteVerb class test (complete·verb_text)");

 		auto verb= new CompleteVerb
 								(	
 									"rolled"  // verb
 								);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Complete);
 		assert (verb.syntax_type        == Syntax.Type.CompleteVerb);
 		assert (verb.items         is null);
 		assert (verb.text          == "rolled");
 		assert (verb.toString      == "rolled");
 		assert (verb.otag          == "<completeverb>");
 		assert (verb.ctag          == "</completeverb>");
	}
}

class PassiveVerb :ActionVerb
{	// contents:
	// · helping·verb + active·verb

	//this (in string verb_text)
	//{
	//	super (ActionType.Passive, Syntax.Type.PassiveVerb, verb_text);
	//}

	this (in Verb[] verb_items)
	{
		super (ActionType.Passive, Syntax.Type.PassiveVerb, verb_items);
	}

	//this (in Container verb_items)
	//{
	//	super (ActionType.Passive, Syntax.Type.PassiveVerb, verb_items);
	//}

	unittest
	{
 		stdout.writeln("ActionVerb class test 3 ([helping·verb, active·verb])");

 		auto verb= new PassiveVerb
						(	[	new HelpingVerb ("was"),
								new ActiveVerb  ("read")
							]
						);

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Passive);
 		assert (verb.syntax_type        == Syntax.Type.PassiveVerb);
 		assert (verb.toString      == "was read");
 		assert (verb.otag          == "<passiveverb>");
 		assert (verb.ctag          == "</passiveverb>");
	}
}

class LinkingVerb :Verb
{	// contents:
	// · lionking·verb 
	// · helping·verb + linking·verb
	// 
	// denote the state or condition of the subject
	// : link the subject to:
	//   · a noun that renames the subject (predicate·noun) or
	//   · a adjective that describes the subject (predecate·adjective)

	//const Language subject;  // noun, pronoun
	//const Language  state;   // adjective, noun, ...

	this (in string verb_text)
	{
		super (Verb.Type.Linking, Syntax.Type.LinkingVerb, verb_text);
		//this.subject= subject;
		//this.state  = state;
	}

	this (in Verb[] verb_items)
	{
		super (Verb.Type.Linking, Syntax.Type.LinkingVerb, verb_items);
		//this.subject= subject;
		//this.state  = state;
	}

	//this (in Container verb_items)
	//{
	//	super (Verb.Type.Action, Syntax.Type.LinkingVerb, verb_items);
	//}

	unittest 
	{
 		stdout.writeln("LinkingVerb class test 1: subject, verb_items, state");

 		auto verb= new LinkingVerb
 								([  // verb·phrase
		 							new HelpingVerb ("did"),	
		 							new LinkingVerb ("taste")
		 						]);

 		assert (verb.language_type == Language.Type.Syntax);
 		assert (verb.syntax_type        == Syntax.Type.LinkingVerb);
		assert (verb.verb_type     == Verb.Type.Linking);
 		assert (verb.toString      == "did taste");
 		assert (verb.otag          == "<linkingverb>");
 		assert (verb.ctag          == "</linkingverb>");
	}

	unittest
	{
 		stdout.writeln("LinkingVerb class test 2: subject, verb, noun");

 		auto verb= new LinkingVerb ("is" );

 		assert (verb.language_type == Language.Type.Syntax);
		assert (verb.verb_type     == Verb.Type.Linking);
 		assert (verb.syntax_type        == Syntax.Type.LinkingVerb);
 		assert (verb.toString      == "is");
 		assert (verb.otag          == "<linkingverb>");
 		assert (verb.ctag          == "</linkingverb>");
	}

	/*unittest
	{
 		stdout.writeln("ActionVerb class test 3: complete, text");

 		auto verb= new ActionVerb
 								(	ActionType.Complete, 
 									new Container  
										([  // subject
											new Determiner (Determiner.Type.Definite),
											new Separator  (Syntax.Type.Blankspace),
		 									new Noun (	Noun.Type.Singular,
		 												"ball"
		 											 )
		 								]),
 									"rolled"  // verb
 								);

 		assert (verb.language_type == Language.Type.Syntax);
 		assert (verb.type        == Syntax.Type.Verb);
		assert (verb.verb_type     == Verb.Type.Action);
		assert (verb.action_type   == ActionVerb.ActionType.Complete);
 		assert (verb.items         is null);
 		assert (verb.text          == "rolled");
 		assert (verb.toString      == "rolled");
 		assert (verb.otag          == "<actionverb>");
 		assert (verb.ctag          == "</actionverb>");
	}*/
}

class HelpingVerb: Verb 
{	// contents:
	// · helping·verb 
	// · helping·verbs

	this (in HelpingVerb[] verb_items)
	{
		super (Verb.Type.Helping, Syntax.Type.HelpingVerb, verb_items);
	}

	this (in string verb_text)
	{
		super (Verb.Type.Helping, Syntax.Type.HelpingVerb, verb_text);
	}

	//this (in Container verb_items)
	//{
	//	super (Verb.Type.Helping, Syntax.Type.HelpingVerb, verb_items);
	//}

	unittest
	{
		stdout.writeln("HelpingVerb class test (helping·verb_text)");

		auto verb= new HelpingVerb
							(	"will"
							);

		assert (verb.language_type   == Language.Type.Syntax);
		assert (verb.verb_type       == Verb.Type.Helping);
		assert (verb.syntax_type          == Syntax.Type.HelpingVerb);
		assert (verb.text            == "will");
		assert (verb.toString        == "will");
		assert (verb.otag            == "<helpingverb>");
		assert (verb.ctag            == "</helpingverb>");
	}
}


// --- Container :Language ---
class Container :Language
{	// multple items with one or more grammatical elements

	const     string text_separator; 
	const Language[] items;

	@disable // Language
		this (in Language.Type type, in string text);

	this (in Language[] items, in string text_separator= Syntax.blankspace) 
	{
		super (Language.Type.Container, null);
		this.text_separator= text_separator;
		this.items    = items;
	}

	this (in Language item) 
	{
		super (Language.Type.Container, null);
		this.text_separator= null;
		this.items    = [item];
	}

	override // language
	string toString() const 
	{
		string text = "";

		if (text_separator is null)
		{
			foreach (ref item; items) 
			{
				text~= item.toString;
			}
		}
		else
		{
			auto items_length= items.length;
			if (items_length > 0)
			{
				auto last_index= items_length - 1;
				text~= items[0].toString;
				int index= 1;
				while (index <= last_index)
				{
					auto item            = items[index];
					auto item_string     = item.toString;
					auto punctuation_item= (			item.language_type == Language.Type.Syntax &&
											(cast(Syntax)item).syntax_type == Syntax.Type.Punctuation);

					text~= punctuation_item? item_string : (text_separator ~ item_string);
					index++;
				}
			}
		}
		return text;
	}	

	unittest
	{
		stdout.writeln("Container class test 1 (Language[])");

		auto container= new Container(
							[
								new Language (Language.Type.Syntax, "unit"),
								new Syntax ("test", Syntax.Type.Noun),
								new Syntax ("none") // TODO: shouldnt be allowed in a container
							]);
		assert (container.language_type == Language.Type.Container);
		assert (container.text          is null);
		assert (container.toString      == "unit test none");
		assert (container.otag          == "<container>");
		assert (container.ctag          == "</container>");

		auto lang0= container.items[0];
		auto lang1= container.items[1];
		auto lang2= container.items[2];

		assert (lang0.text == "unit");
		assert (lang0.language_type == Language.Type.Syntax);

		assert (lang1.language_type == Language.Type.Syntax);
		auto syntax0= cast(Syntax)lang1;
		assert (syntax0.syntax_type == Syntax.Type.Noun);

		assert (lang2.language_type == Language.Type.Syntax);
		auto syntax1= cast(Syntax)lang2;
		assert (syntax1.syntax_type   == Syntax.Type.none);
		assert (syntax1.toString == "none");
	}

	unittest
	{
		stdout.writeln("Container class test 2 (Language)");

		auto container= new Container( new Syntax ("test", Syntax.Type.Noun));

		assert (container.language_type == Language.Type.Container);
		assert (container.text          == null);
		//writeln(container.toString);
		assert (container.toString      == "test");
		assert (container.otag          == "<container>");
		assert (container.ctag          == "</container>");

		auto lang0= container.items[0];
		assert (lang0.text == "test");
		assert (lang0.language_type == Language.Type.Syntax);
		}
}

class Phrase :Container 
{	// multiple items with one grammatical element
	// a group of words without a subject-verb component

	enum Type
	{
		Noun,
		Verb,
		Adjective,
		Adverb,
		Preposition,
		Gerund,
		Participle,
		none
	}
	const Type phrase_type;

	this (	in     Type type, 
			in Syntax[] items) 
	{
		super (items);
		this.phrase_type= type;
	}

	this (	in Syntax[] items,
		 	in     Type type= Type.none) 
	{
		this (type, items);
	}

	unittest
	{
		stdout.writeln("Phrase class test 1 (noun·phrase)");

		auto phrase= 
		new Phrase
		( 
			Phrase.Type.Noun,
		 	[new Adjective ("best"),
		 	 new CommonNoun ("friend")
		 	]
		);

		assert (phrase.language_type == Language.Type.Container);
		assert (phrase.phrase_type   == Phrase.Type.Noun);
		assert (phrase.text          == "");
		auto item1= cast(Syntax)phrase.items[1];
		assert (item1.syntax_type == Syntax.Type.Noun);
		assert (item1.toString == "friend");
		assert (item1.tagname == "commonnoun");
	}

	unittest
	{
		stdout.writeln("Phrase class test 2 (none)");

		auto phrase= 
		new Phrase
		( 
		 	[new Adjective ("random"),
		 	 new CommonNoun ("stuff")
		 	]
		);

		assert (phrase.language_type == Language.Type.Container);
		assert (phrase.phrase_type   == Phrase.Type.none);
	}
}

class NounPhrase :Phrase 
{
	@disable 
		this (in Syntax[] items, Type type);

	this (	in Syntax[] items)
	{
		super (Type.Noun, items);
	}

	unittest
	{
		stdout.writeln("NounPhrase class test");

		auto phrase= 
		new NounPhrase
		( 
		 	[new  Adjective ("best"),
		 	 new CommonNoun ("friend")
		 	]
		);

		assert (phrase.language_type == Language.Type.Container);
		assert (phrase.phrase_type   == Phrase.Type.Noun);
		assert (phrase.toString      == "best friend");
	}
}

class VerbPhrase :Phrase 
{
	@disable 
		this (in Syntax[] items, Type type);

	this (	in Syntax[] items)
	{
		super (Type.Verb, items);
	}

	unittest
	{
		stdout.writeln("VerbPhrase class test");

		auto phrase= 
		new VerbPhrase
		( 
		 	[new HelpingVerb ("was"),
		 	 new  ActiveVerb ("working")
		 	]
		);

		assert (phrase.phrase_type   == Phrase.Type.Verb);
		assert (phrase.toString      == "was working");
	}
}

class AdjectivePhrase :Phrase 
{
	@disable 
		this (in Syntax[] items, Type type);

	this (	in Syntax[] items)
	{
		super (Type.Adjective, items);
	}

	unittest
	{
		stdout.writeln("AdjectivePhrase class test");

		auto phrase= 
		new AdjectivePhrase
		( 
		 	[new    Adverb ("very"),
		 	 new Adjective ("pretty")
		 	]
		);

		assert (phrase.phrase_type == Phrase.Type.Adjective);
		assert (phrase.toString    == "very pretty");
	}
}

class AdverbPhrase :Phrase 
{
	@disable 
		this (in Syntax[] items, Type type);

	this (	in Syntax[] items)
	{
		super (Type.Adverb, items);
	}

	unittest
	{
		stdout.writeln("AdverbPhrase class test");

		auto phrase= 
		new AdverbPhrase
		( 
		 	[new Adverb ("really"),
		 	 new Adverb ("slowly")
		 	]
		);

		assert (phrase.phrase_type   == Phrase.Type.Adverb);
		assert (phrase.toString      == "really slowly");
	}
}

class PrepositionPhrase :Phrase 
{
	@disable 
		this (in Syntax[] items, Type type);

	this (	in Syntax[] items)
	{
		super (Type.Preposition, items);
	}

	unittest
	{
		stdout.writeln("PrepositionPhrase class test");

		auto phrase= 
		new PrepositionPhrase
		( 
		 	[new Preposition ("in"),
		 	 new  Determiner (Determiner.Type.Definite),
		 	 new  CommonNoun ("bin")
		 	]
		);

		assert (phrase.phrase_type   == Phrase.Type.Preposition);
		assert (phrase.toString      == "in the bin");
	}
}

class GerundPhrase :Phrase 
{
	this (	in Syntax[] items)
	{
		super (Type.Gerund, items);
	}

	unittest
	{
		stdout.writeln("GerundPhrase class test");

		auto phrase= 
		new GerundPhrase
		( 
		 	[new     Gerund ("running"),
		 	 new CommonNoun ("marathons")
		 	]
		);

		assert (phrase.language_type == Language.Type.Container);
		assert (phrase.phrase_type   == Phrase.Type.Gerund);
		assert (phrase.toString      == "running marathons");
	}
}

class ParticiplePhrase :Phrase 
{
	this (	in Syntax[] items)
	{
		super (Type.Participle, items);
	}

	unittest
	{
		stdout.writeln("ParticiplePhrase class test");

		auto phrase= 
		new ParticiplePhrase
		( 
		 	[new Participle ("throwing"),
		 	 new CommonNoun ("rocks")
		 	]
		);

		assert (phrase.phrase_type   == Phrase.Type.Participle);
		assert (phrase.toString      == "throwing rocks");
	}
}

class Clause :Container
{	// central class of the Clause hierarchy
	// . the DependentClause, IndependentClause subclasses are for type and structure depiction
	//   in toString()
	//
	// a clause is a container of language (containers and type)
	
	// inherit constructors
	this (Args...)(auto ref Args args)
	{ 
		super(args);
	}


	/*override
	string toString() const 
	{
		auto subject_text= subject.toString;
		auto verbs_text  = super.toString;
		return	subject_text ~ 
				((subject_text.length > 0 && verbs_text.length > 0)? text_separator : "") ~
		  		verbs_text;
	}*/

	/*unittest
	{
		stdout.writeln("Clause class test 1 (Subject subject, VerbPhrase active·verb·phrase )");

		auto clause= new Clause 
							([	new ClauseSubject // subject
									(	new PersonalPronoun ("i")),
								new ClauseActivity // verb
									([	new HelpingVerb ("will"),
										new ActiveVerb  ("be")
									])
							]);

		assert (clause.language_type == Language.Type.Container);
		assert (clause.text          == "");
		assert (clause.toString      == "i will be");
		assert (clause.otag          == "<clause>");
		assert (clause.ctag          == "</clause>");

		auto lang_action= clause.items[1];
		assert (lang_action.language_type == Language.Type.Container);
		assert (lang_action.otag == "<activity>");

		auto lang_helping_verb= (cast(ClauseActivity)lang_action).items[0];
		assert (lang_helping_verb.language_type == Language.Type.Syntax);
		auto syntax_helping_verb= cast(Syntax)lang_helping_verb;
		assert (syntax_helping_verb.syntax_type == Syntax.Type.HelpingVerb);
		auto helping_verb= cast(HelpingVerb)syntax_helping_verb;
		assert (helping_verb.toString  == "will");
		assert (helping_verb.ctag      == "</helpingverb>");

		auto lang_active_verb= (cast(ClauseActivity)lang_action).items[1];
		assert (lang_active_verb.language_type == Language.Type.Syntax);
		auto syntax_active_verb= cast(Syntax)lang_active_verb;
		assert (syntax_active_verb.syntax_type == Syntax.Type.ActiveVerb);
		auto active_verb= cast(ActiveVerb)lang_active_verb;
		assert (active_verb.syntax_type    == Syntax.Type.ActiveVerb);
		assert (active_verb.toString  == "be");
		assert (active_verb.ctag      == "</activeverb>");
	}*/

	/*unittest
	{
		stdout.writeln("Clause class test 2 (ClauseSubject subject + ClauseActivity activity)");

		auto clause= new Clause 
							([	new ClauseSubject 
									(	new PersonalPronoun ("i")),
								new ClauseActivity
									([	new HelpingVerb ("will"),
										new ActiveVerb  ("be")
									])
							]);

		assert (clause.language_type == Language.Type.Container);
		assert (clause.text          == "");
		assert (clause.toString      == "i will be");
		assert (clause.otag          == "<clause>");
		assert (clause.ctag          == "</clause>");

		auto lang_activity= clause.items[1];
		assert (lang_activity.language_type == Language.Type.Container);
		assert (lang_activity.otag == "<activity>");

		auto activity= cast(ClauseActivity)lang_activity;
		assert (activity.items.length == 2);
		assert (activity.toString == "will be");
		assert ((cast(Verb)activity.items[0]).verb_type == Verb.Type.Helping);
		assert ((cast(Verb)activity.items[0]).syntax_type    == Syntax.Type.HelpingVerb);
		assert ((cast(Verb)activity.items[1]).verb_type == Verb.Type.Action);
		assert ((cast(Verb)activity.items[1]).syntax_type    == Syntax.Type.ActiveVerb);
		auto syntax_active_verb= cast(ActiveVerb)activity.items[1];
		auto        active_verb= cast(ActiveVerb)syntax_active_verb;
		//(cast(ActiveVerb)action.items[1]);
		assert (active_verb.items == null);
		assert (active_verb.text == "be");

		//assert (lang_verb.items[0].language_type == Language.Type.Syntax);
		//auto syntax_verb_item= (cast(Syntax)lang_verb.items[0]);
		//assert (syntax_verb_item.type == Syntax.Type.Verb);

		//auto verb_helping_verb= (cast(Verb)syntax_verb_item).items[0];
		//assert (lang_helping_verb.language_type == Language.Type.Syntax);
		//auto syntax_helping_verb= cast(Syntax)lang_helping_verb;
		//assert (syntax_helping_verb.type == Syntax.Type.HelpingVerb);
		//auto helping_verb= cast(HelpingVerb)syntax_helping_verb;
		//assert (helping_verb.toString  == "will");
		//assert (helping_verb.ctag      == "</helpingverb>");

		//auto lang_active_verb= (cast(VerbPhrase)lang_verb_phrase).items[1];
		//assert (lang_active_verb.language_type == Language.Type.Syntax);
		//auto syntax_active_verb= cast(Syntax)lang_active_verb;
		//assert (syntax_active_verb.type == Syntax.Type.ActiveVerb);
		//auto active_verb= cast(ActiveVerb)lang_active_verb;
		//assert (active_verb.type    == Syntax.Type.ActiveVerb);
		//assert (active_verb.toString  == "be");
		//assert (active_verb.ctag      == "</activeverb>");
	}*/

	unittest
	{
		stdout.writeln("Clause class test 3 (independent·clause + dependent·clause)");
		// simpler than test 2

		auto clause= new Clause 
							([	new Clause  // independent·clause
								([	new PersonalPronoun ("i"),
									new ActiveVerb      ("love"),
									new Determiner      (Determiner.Type.Definite),
									new CommonNoun      ("person")
								]),
								new Clause  // dependent·clause
								([	new RelativePronoun ("who"),
									new ActiveVerb      ("cleaned"),
									new Determiner      (Determiner.Type.Definite),
									new CommonNoun      ("house")
								])
							]);
	}

	unittest
	{
		stdout.writeln("Clause class test 4 (Colin walked into the house that had been sold)");

		auto clause= new Clause 
							([	new Clause  // independent·clause
								([	new ProperNoun ("Colin"),
									new ActiveVerb ("walked"),
									new Preposition ("into"),
									new Determiner (Determiner.Type.Definite),
									new CommonNoun ("house")
								]),
								new Clause  // dependent·clause
								([	new RelativePronoun ("that"),
									new PassiveVerb
										([	new HelpingVerb("had been"),
											new ActiveVerb("sold")
										])
								])
							]);
	}

	unittest
	{
		stdout.writeln("Clause class test 5 (teachers who are extra nice are paid double)");

		auto clause= new Clause 
							([	new Clause  // independent·clause
								([	new CommonNoun ("teachers"),
									new Clause   // dependent·clause
									([	new RelativePronoun ("who"),  
										new ActiveVerb ("are"),
										new Adverb ("extra nice")
									]),
									new PassiveVerb
									([	new HelpingVerb ("are"),
										new ActiveVerb ("paid")
									]),
									new Adverb ("double")
								])
							]);
	}

	unittest
	{
		stdout.writeln("Clause class test 6 (i spoke with the boy whom drew that picture)");

		auto clause= new Clause 
						([	new IndependentClause
							([	new PersonalPronoun ("i"),
								new ActiveVerb      ("spoke"),
								new Preposition     ("with"),
								new Determiner      (Determiner.Type.Definite),
								new CommonNoun      ("boy")
							]),
							new DependentClause
							([	new RelativePronoun ("whom"),
								new ActiveVerb      ("drew"),
								new Preposition     ("that"),
								new CommonNoun      ("picture")
							])
						]);

		{
			auto lang_indep_clause= clause.items[0];
			assert (lang_indep_clause.language_type == Language.Type.Container);
			assert (lang_indep_clause.tagname       == "independentclause");
			auto indep_clause= cast(Clause)lang_indep_clause;
			auto syntax      = cast(Syntax)indep_clause.items[0];
			assert (syntax.syntax_type == Syntax.Type.Pronoun);
		}

		{
			auto lang_dep_clause= clause.items[1];
			assert (lang_dep_clause.tagname == "dependentclause");
			auto syntax= cast(Syntax)(cast(Clause)lang_dep_clause).items[2];
			assert (syntax.otag == "<preposition>");
		}
	}

} // Clause

class IndependentClause :Clause 
{	
	// inherit constructors
	this (Args...)(auto ref Args args)
	{ 
		super(args);
	}
}

class DependentClause :Clause 
{	
	// inherit constructors
	this (Args...)(auto ref Args args)
	{ 
		super(args);
	}
}



class Sentence :Container
{	// complete thought

	//enum Type
	//{
	//	Simple,
	//	Compound,
	//	Complex,
	//	Compoundcomplex
	//}

	//immutable Type sentence_type;

	// inherit constructors
	this (Args...)(auto ref Args args)
	{ 
		super(args);
	}

	/*this (in Type type, Language[] items)
	{	// contents: clauses, separators, punctuation
		super (items);

		//cannot implicitly convert expression `type` of type `const(Type)` to `immutable(Type)`
		this.sentence_type= type;
	}*/

	unittest
	{		
		stdout.writeln("Sentence class test (clause, syntax, clause)");

		auto sentence= 
		new Sentence 
		([//Sentence.Type.Complex,
			new IndependentClause 
			(//Clause.Type.Independent,
				[
					new PersonalPronoun     ("i"),
					new ActiveVerb  ("kicked"),
					new Determiner  (Determiner.Type.Definite),
					new Adjective   ("red"),

					new Noun        
					(	Noun.Type.Common,
						[ new CommonNoun ("ball"),
						  new CommonNoun ("tack")
						]
					)
				]
			),
			new Punctuation (Punctuation.Type.comma),
			new Conjunction ("and"),
			new IndependentClause
			(//Clause.Type.independent,
				[
					new PersonalPronoun ("it"),
					new ActiveVerb      ("hit"),
					new ProperNoun      ("Tom"),
					new Adverb          ("hard")
				]
			)			
		]);

		//assert (sentence.language_type == Language.Type.Language);
		assert (sentence.text          == "");
		//writeln("sentence_string: '", sentence.toString, "'");
		auto sentence_string= sentence.toString;
		auto my_string= "i kicked the red ball tack, and it hit Tom hard";
		//writefln("sentence_string.length: %d, my_string.length: %d", sentence_string.length, my_string.length);
		//for (auto i= 0; i < my_string.length; i++)
		//{
		//	if (my_string[i] == sentence_string[i])
		//	{
		//		writeln(my_string[i]);
		//	}
		//	else
		//	{
		//		writeln("'", my_string[i], "' ",  "'", sentence_string[i], "'");
		//	}
		//}
		//assert (sentence_string.length == my_string.length);
		//for (i=0; i < mystring.length)   i kicked the red ball tack, and it hit Tom hard
		assert (sentence.toString      == "i kicked the red ball tack, and it hit Tom hard");
		assert (sentence.otag          == "<sentence>");
		assert (sentence.ctag          == "</sentence>");

		//assert ((cast(Clause)sentence.items[0]).clause_type     == Syntax.Type.Independent);
		assert ((cast(Punctuation)sentence.items[1]).syntax_type == Syntax.Type.Punctuation);
		//assert ((cast(Clause)sentence.items[$ - 1]).clause_type == Syntax.Type.Independent);

	}
}


struct SpeechDisplay
{
	enum Shape 
	{
		bold      = 0b0001,
		italic    = 0b0010,
		underline = 0b0100
	}
	enum Color 
	{_default, black, grey, orange, red, green, darkgreen, purple, blue, yellow, magenta, cyan, tan, white}

	RGB   rgb;
	int   shape;

	static RGB rgb_black, background_color= RGB(0,0,0);
	static RGB                   rgb_white= RGB(255,255,255);

	static SpeechDisplay[Syntax.Type] display;

	static RGB rgb_for_color(in SpeechDisplay.Color color) 
	{
		final switch (color)
		{
			case Color.black:
				return rgb_black;
			case Color.grey, Color._default:
				return RGB(160,160,160);
			case Color.orange:
				return RGB(255,165,128);
			case Color.red:
				return RGB(255,0,0);
			case Color.green:
				return RGB(0,255,0);
			case Color.darkgreen:
				return RGB(0,153,76);
			case Color.purple:
				return RGB(204,153,255);
			case Color.blue:
				return RGB(51,153,255);
			case Color.yellow:
				return RGB(255,255,0);
			case Color.magenta:
				return RGB(255,0,255);
			case Color.cyan:
				return RGB(0,255,255);
			case Color.tan:
				return RGB(161,108,108); //RGB(179,162,162);
			case Color.white:
				return rgb_white;
		}
	}



	//this (in SpeechDisplay.Color color, RGB rgb, int shape)
	//{
	//	this.color= color;
	//	this.rgb  = rgb;
	//	this.shape= shape;
	//}
	static this() 
	{
		// all Syntax.Color constants must appear here
		display[Syntax.Type.Determiner]  = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.orange));
		display[Syntax.Type.Noun]        = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.blue));
		display[Syntax.Type.Pronoun]     = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.yellow),    SpeechDisplay.Shape.bold);
		display[Syntax.Type.Adjective]   = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.magenta));
		display[Syntax.Type.Verb]        = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.green),     SpeechDisplay.Shape.italic);
		display[Syntax.Type.ActiveVerb]  = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.green),     SpeechDisplay.Shape.italic);
		display[Syntax.Type.CompleteVerb]= SpeechDisplay(rgb_for_color(SpeechDisplay.Color.green),     SpeechDisplay.Shape.italic);
		display[Syntax.Type.PassiveVerb] = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.green),     SpeechDisplay.Shape.italic);
		display[Syntax.Type.LinkingVerb] = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.green),     SpeechDisplay.Shape.italic);
		display[Syntax.Type.HelpingVerb] = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.darkgreen), SpeechDisplay.Shape.italic);
		display[Syntax.Type.Adverb]      = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.cyan),      SpeechDisplay.Shape.italic);
		display[Syntax.Type.Preposition] = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.purple));
		display[Syntax.Type.Conjunction] = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.tan));
		display[Syntax.Type.Blankspace]  = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.grey));
		display[Syntax.Type.Linkspace]   = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.grey));
		display[Syntax.Type.Punctuation] = SpeechDisplay(rgb_for_color(SpeechDisplay.Color.white));
	}
} // SpeechDisplay



struct Geometry 
{
	int      width, height;
	int origin_col, origin_row;
}

struct WindowArea
{
	App*      app;
	Terminal* terminal;

	Geometry geometry;
	int posX, posY;

	this (App* app, Terminal* terminal)
	{
		this.app     = app;
		this.terminal= terminal;
	}

	bool valid_coord(in int col, in int row)
	{
		return col >= 0 && col < geometry.width &&
		       row >= 0 && row < geometry.height;
	}

	void moveTo (in int area_col, in int area_row)
	{
		if (valid_coord(area_col, area_row))
		{
			int window_col, window_row;
			area_coord_to_window_coord (area_col, area_row, window_col, window_row);
			terminal.moveTo (window_col, window_row);
			posX= area_col;
			posY= area_row;
		}
		else
		{
			stderr.writef("WindowArea.moveTo: invalid coords: X= %d, Y= %d\n", area_col, area_row);
			//_ok= false;
		}
	}

	void window_coord_to_area_coord(in int window_col, in int window_row, out int out_col, out int out_row) 
	{
		out_col= window_col - geometry.origin_col;
		out_row= window_row - geometry.origin_row;

	}

	void area_coord_to_window_coord(in int area_col, in int area_row, out int out_col, out int out_row)
	{
		out_col= area_col + geometry.origin_col;
		out_row= area_row + geometry.origin_row;
	}

	void write_text (in string text, in int area_col = -1, in int area_row = -1)
	{

		if (area_col >= 0)
		{
			assert (area_row >= 0);

			this.posX= area_col;
			this.posY= area_row;
		}

		auto terminal_width = terminal.width;
		auto terminal_height= terminal.height;

		moveTo (this.posX, this.posY);

		terminal.write (text);
		//terminal.flush;   // DEBUG
		window_coord_to_area_coord (terminal.cursorX, terminal.cursorY, this.posX, this.posY);
	}

	void write_syntax (const Syntax syntax)
	{
		
		const auto type_display= SpeechDisplay.display[syntax.syntax_type];
		terminal.setTrueColor(type_display.rgb, SpeechDisplay.background_color);

		terminal.bold      (SpeechDisplay.Shape.bold      == (type_display.shape & SpeechDisplay.Shape.bold));
		terminal.italic    (SpeechDisplay.Shape.italic    == (type_display.shape & SpeechDisplay.Shape.italic));
		terminal.underline (SpeechDisplay.Shape.underline == (type_display.shape & SpeechDisplay.Shape.underline));

		write_text (syntax.toString);

		terminal.reset();
		
	}

	void write_language (in Language language) 
	{	stdout.writeln("in write_language");

		int indentX;

		void _write_language (in Language language) 
		{	import std.array: replicate;
			stdout.writeln("  in _write_language");


			final switch (language.language_type)
			{
				case Language.Type.Container:
					{
						void
						_write_indent ()
						{
							write_text (replicate(" ", indentX * 4));
						}

						auto container= (cast(Container)language);

						app.set_text_color(SpeechDisplay.Color._default);
						write_text (container.otag ~ "\n");

						indentX++;
						stdout.writeln("indentX == ", indentX);

						foreach (ref container_language; container.items)
						{
							_write_indent ();
							_write_language (container_language);
							write_text ("\n");
						}

						indentX--;
						stdout.writeln("indentX == ", indentX);

						app.set_text_color(SpeechDisplay.Color._default);
						_write_indent ();
						write_text (container.ctag);
					}
					break;


				case Language.Type.Syntax:
					{
						app.set_text_color(SpeechDisplay.Color._default);
						auto syntax= cast(Syntax)language;
						write_text   (syntax.otag);

						if (syntax.items != null)
						{
							foreach (i, ref item; syntax.items)
							{
								write_syntax (item);
								if ((i + 1) < syntax.items.length)
								{
									write_text (syntax.text_separator);
								}
							}
						}
						else
						{
							write_syntax (syntax);
						}

						app.set_text_color(SpeechDisplay.Color._default);			
						write_text   (syntax.ctag);
					}
					break;
			}
			stdout.writeln("  out _write_language");
		}

		_write_language (language);

		stdout.writeln("out write_language");
	}


} // WindowArea

struct App 
{
	Terminal* terminal;
	RealTimeConsoleInput input;

	int window_width, window_height;
	WindowArea output_area, input_area, utility_area;


	this (Terminal* terminal) 
	{
		this.terminal= terminal;

		this.input= RealTimeConsoleInput(this.terminal, ConsoleInputFlags.raw| 
														ConsoleInputFlags.allInputEvents);
		this.window_width = terminal.width;
		this.window_height= terminal.height;

	}

	void set_text_color (in SpeechDisplay.Color color) 
	{
		RGB pen_color= SpeechDisplay.rgb_for_color(color);
		this.terminal.setTrueColor(pen_color, SpeechDisplay.background_color);
	}

	void lay_out_ui () 
	{
		const   input_area_height= 10;
		const utility_area_height= 1;

		auto problem= true;
		while (problem)
		{
			{
				  input_area= WindowArea (&this, terminal);
				 output_area= WindowArea (&this, terminal);
				utility_area= WindowArea (&this, terminal);

				output_area.geometry = Geometry(this.window_width, 
											 this.window_height - input_area_height - utility_area_height, 0, 0);

				utility_area.geometry= Geometry(this.window_width, utility_area_height, 0,
				                             this.window_height - input_area_height + utility_area_height);

				input_area.geometry  = Geometry(this.window_width, input_area_height, 0,
				                             this.window_height - input_area_height
				                                                + utility_area_height + 1);
				// unittest set_text_color(SpeechDisplay.Color.white);

				terminal.clear;

				// TODO: do not render if areas have invalid geometry (user must resize)

				import std.array: replicate;
				utility_area.moveTo(0,0);
				utility_area.write_text(replicate("🀫", utility_area.geometry.width));

				//input_area.moveTo(0,5);
				//input_area.write_text("herllo");

				input_area.write_text ("this is where you write\n", 0, 0);
				input_area.write_text ("but for now you can only (Q)uit");
			}

			{
				//Language[] language_;
				/*auto language= 
				
				new Sentence 
					(Sentence.Type.complex,
					[new Clause 
						(Clause.Type.independent,
						[
							new Word        (Syntax.Type.pronoun, "i"),
							new Separator   (Syntax.Type.blankspace),
							new Word        (Syntax.Type.verb, "kicked"),
							new Separator   (Syntax.Type.blankspace),
							new Word        (Syntax.Type.determiner, "the"),
							new Separator   (Syntax.Type.blankspace),
							new Word        (Syntax.Type.adjective, "red"),
							new Separator   (Syntax.Type.blankspace),


							new Noun        
								([new Word      (Syntax.Type.noun, "ball"),
								  new Separator (Syntax.Type.blankspace),
								  new Word      (Syntax.Type.noun, "tack")]),




						]),
					new Punctuation (Punctuation.Type.comma),
					new Separator   (Syntax.Type.blankspace),
					new Conjunction (Conjunction.Type.AND),
					new Separator   (Syntax.Type.blankspace),
					new Clause
						(Clause.Type.independent,
						[
							new Word        (Syntax.Type.pronoun, "it"),
							new Separator   (Syntax.Type.blankspace),
							new Word        (Syntax.Type.verb, "hit"),
							new Separator   (Syntax.Type.blankspace),
							new Word        (Syntax.Type.noun, "Tom"),
							new Separator   (Syntax.Type.blankspace),
							new Word        (Syntax.Type.adverb, "hard")
						]),
					]);
				*/
				{
					auto clause= new Clause 
								([	new Clause  // independent·clause
									([	new PersonalPronoun ("i"),
										new ActiveVerb      ("love"),
										new Determiner      (Determiner.Type.Definite),
										new CommonNoun      ("person")
									]),
									new Clause  // dependent·clause
									([	new RelativePronoun ("who"),
										new ActiveVerb      ("cleaned"),
										new Determiner      (Determiner.Type.Definite),
										new CommonNoun      ("house")
									])
								]);

					output_area.moveTo(0, 0);
					output_area.write_language(clause);
				}
				/*{
					output_area.write_text ("\n\n");
					auto clause= new Clause 
						([	new Clause  // independent·clause
							([	new CommonNoun ("teachers"),
								new Clause   // dependent·clause
								([	new RelativePronoun ("who"),  
									new ActiveVerb ("are"),
									new Adverb ("extra nice")
								]),
								new PassiveVerb
								([	new HelpingVerb ("are"),
									new ActiveVerb ("paid")
								]),
								new Adverb ("double")
							])
						]);
					output_area.write_language(clause);					
				}
				{
					output_area.write_text ("\n\n");
					auto sentence= 
					new Sentence 
					([//Sentence.Type.Complex,
						new IndependentClause 
						(//Clause.Type.Independent,
							[
								new PersonalPronoun     ("i"),
								new ActiveVerb  ("kicked"),
								new Determiner  (Determiner.Type.Definite),
								new Adjective   ("red"),

								new Noun        
								(	Noun.Type.Common,
									[ new CommonNoun ("ball"),
									  new CommonNoun ("tack")
									]
								)
							]
						),
						new Punctuation (Punctuation.Type.comma),
						new Conjunction ("and"),
						new IndependentClause
						(//Clause.Type.independent,
							[
								new PersonalPronoun ("it"),
								new ActiveVerb      ("hit"),
								new ProperNoun      ("Tom"),
								new Adverb          ("hard")
							]
						)			
					]);
					output_area.write_language(sentence);										
				}
				{
					output_area.write_text ("\n\n");
					auto clause=
					new Clause
						([	new Gerund ("running"),
							//([ new ActiveVerb ("running")
							//]),
							new CommonNoun ("marathons"),
							new ActiveVerb ("is"),
							new Adjective  ("fun")
						]);
					output_area.write_language(clause);										
				}
				{
					output_area.write_text ("\n\n");
					auto phrase= 
					new GerundPhrase
					( 
					 	[new     Gerund ("running"),
					 	 new CommonNoun ("marathons")
					 	]
					);
					output_area.write_language(phrase);											
				}*/
				{
					output_area.write_text ("\n\n");
					auto clause= 
					new Clause
					([	new ParticiplePhrase
						([	new Participle ("throwing"),
						 	new CommonNoun ("rocks")
						]),
						new PrepositionPhrase
						([	new Preposition ("across"),
							new Determiner (Determiner.Type.Definite),
							new CommonNoun ("water")
						])
					]);
					output_area.write_language(clause);
				}
			}

			/*{
				Language[] language_ = [new Word           (Syntax.Type.pronoun, "this"),
									    new Separator      (Syntax.Type.blankspace),
									    new Word           (Syntax.Type.verb, "is"),
									    new Separator      (Syntax.Type.blankspace),
										new Word           (Syntax.Type.determiner, "a"),
									    new Separator      (Syntax.Type.blankspace),
									    new Word           (Syntax.Type.noun, "phrase"),
									    new Punctuation    (Punctuation.Type.colon),
									    new Separator      (Syntax.Type.blankspace),
									    new Phrase         (Syntax.Type.noun,
									    	[new Word      (Syntax.Type.noun, "word1"),
									    	 new Separator (Syntax.Type.linkspace),
									    	 new Word      (Syntax.Type.noun, "word2")
									    	]),
									    ];
				output_area.moveTo(0, 5);
				//terminal.moveTo(0, output_area.geometry.origin_row+1);
				foreach (ref language; language_) 
				{
					output_area.write_language(language);
				}
			}*/

			try 
			{
				terminal.flush();
				input_area.moveTo(0,0);
				problem= false;
			}
			catch (Exception)
			{
				continue;
			}
		}
	} // lay_out_ui


	void run () 
	{
		lay_out_ui();

		auto keep_running = true;
		while (keep_running) 
		{
			terminal.updateSize();
			auto current_window_width  = terminal.width;
			auto current_window_height = terminal.height;

			if (current_window_width != window_width
			|| current_window_height != window_height) 
			{
				terminal.write("size changed!");

				window_width  = current_window_width;
				window_height = current_window_height;

				lay_out_ui();
			}

			auto event = input.nextEvent();
			switch (event.type()) 
			{
				case InputEvent.Type.KeyboardEvent:
					import std.uni: isAlphaNum;
					import std.conv: to;

					auto kbevent = event.get!(InputEvent.Type.KeyboardEvent);
					if(!kbevent.pressed) 
					{
						break;
					}
					//terminal.writef("\t%s", kbevent);
					//terminal.writef(" (%s)", cast(KeyboardEvent.Key) kbevent.which);
					//terminal.writeln();
					if (kbevent.which == 'Q') {
						keep_running = false;
					}

					auto dch = kbevent.which;
					if (isAlphaNum(dch))
					{
						//import std.string: toStringz;

						input_area.write_text(to!string(dch));
					}
					break;

				case InputEvent.Type.MouseEvent: 
					auto mouse_event = event.get!(InputEvent.Type.MouseEvent);

					if( mouse_event.eventType == MouseEvent.Type.Pressed) 
					{
						if(mouse_event.buttons & MouseEvent.Button.Left) 
						{
							terminal.moveTo(mouse_event.x, mouse_event.y);
						}
					}
					break;
				
				default:
					break;
			}
		}
	}
}


void* _terminal;

void main() 
{
 	auto terminal= Terminal(ConsoleOutputType.cellular);
	_terminal    = &terminal;

	auto app= App(&terminal);

	stderr.open("err.log", "w");
	stdout.open("out.log", "w");

	app.run();
}












/*
class ActiveClause :Clause
{	// transitive·active·action

	const Container direct_object;

	this (in Container subject, in Verb[] verb_items, in Container direct_object,
		  in string text_separator= Syntax.blankspace) 
	{
		super (subject, verb_items);
		this.direct_object= direct_object;
	}

	override
	string toString() const 
	{
		auto clause_text       = super.toString;
		auto direct_object_text= direct_object.toString;
		return	clause_text ~ 
				((clause_text.length > 0 && direct_object_text.length > 0)? text_separator : "") ~
		  		direct_object_text;
	}

	unittest
	{
		stdout.writeln("ActiveClause class test: subject, ",
					   "active·verb, direct_object");

		auto clause= new ActiveClause 
							(	new Container // subject
									([	new Pronoun  (Pronoun.Type.Personal, "i")]),
								[	new ActiveVerb  // verb_items
										("kicked")
								],
								new Container // direct·object
									([	new Determiner (Determiner.Type.Definite),
										new Noun       (Noun.Type.Common, "ball")
									])
							);

		assert (clause.language_type == Language.Type.Container);
		writeln(clause.toString);
		assert (clause.toString      == "i kicked the ball");
		assert (clause.otag          == "<activeclause>");
		assert (clause.ctag          == "</activeclause>");
	}
}

class CompleteClause :Clause
{
	this (Args...)(auto ref Args args)
	{ 
		super(args);
	}
}

class PassiveClause :Clause
{	// transitive·passive·action

	const Container prepositioal_phrase;

	this (in Container subject, in Verb[] verb_items, in Container prepositioal_phrase,
		  in string text_separator= Syntax.blankspace)
	{
		super (subject, verb_items);
		this.prepositioal_phrase= prepositioal_phrase;
	}

	this (in Container subject, in PassiveVerb passive_verb, in Container prepositioal_phrase,
		  in string text_separator= Syntax.blankspace)
	{
		super (subject, verb_items);
		this.prepositioal_phrase= prepositioal_phrase;
	}

	override
	string toString() const 
	{
		auto active_clause_text      = super.toString;
		auto prepositioal_phrase_text= prepositioal_phrase.toString;
		return	active_clause_text ~ 
				((active_clause_text.length > 0 && prepositioal_phrase_text.length > 0)? text_separator : "") ~
		  		prepositioal_phrase_text;

	}

	unittest
	{
		stdout.writeln("PassiveClause class test: subject (Container), ",
					   "verb_items, prepositioal_phrase (Syntax[])");

		auto clause= new PassiveClause 
							(	new Container // subject
									([	new Determiner (Determiner.Type.Definite),
										new Noun       (Noun.Type.Common, "ball")
									]),
								new PassiveVerb
									([	// verb_items
										new HelpingVerb ("was"),    // AixVerb
										new ActiveVerb  ("kicked")  // ActionVerb
									]),
								new Container
								([	// prepositioal_phrase
									new Syntax ("by", Syntax.Type.Preposition),
									new Noun   (Noun.Type.Proper, "John")
								])
							);

		assert (clause.language_type == Language.Type.Container);
		writeln(clause.toString);
		assert (clause.toString      == "the ball was kicked by John");
		assert (clause.otag          == "<passiveclause>");
		assert (clause.ctag          == "</passiveclause>");
	}
}

class LinkingClause :Clause
{	// denotes the state or condition of the subject
	// . links the subject to 
	//   - a noun that renames it (predicate·noun) or
	//   - an adjective that describes it (predicate·adjectice)

	const Container predicate;

	this (	in Container subject,
			in    Verb[] verb_items,
			in Container predicate,
			in    string text_separator= Syntax.blankspace)
	{
		super (subject, verb_items);
		this.predicate= predicate;
	}

	override
	string toString() const 
	{
		auto clause_text      = super.toString;
		auto prepositicte_text= predicate.toString;
		return	clause_text ~ 
				((clause_text.length > 0 && prepositicte_text.length > 0)? text_separator : "") ~
		  		prepositicte_text;
	}

	unittest
	{
		stdout.writeln("LinkingClause class test1 (Container subject, ",
					   							 "Verb[] verb_items, ",
					   							 "Container predicate·noun)");

		auto clause= new LinkingClause 
							(	new Container // subject
									([	new Pronoun (Pronoun.Type.Personal, "i")
									]),
								[	// verb_items
									new Verb (Verb.Type.Helping, "am")    // LinkingVerb
								],
								new Container
								([	// predicate·noun
									new Determiner (Determiner.Type.Definite),
									new Noun   (Noun.Type.Common, "bus"),
									new Noun   (Noun.Type.Common, "driver")
								])
							);

		assert (clause.language_type == Language.Type.Container);
		assert (clause.toString      == "i am the bus driver");
		assert (clause.otag          == "<linkingclause>");
		assert (clause.ctag          == "</linkingclause>");
	}

	unittest
	{
		stdout.writeln("LinkingClause class test2 (Container subject, ",
					   							 "Verb[] verb_items, ",
					   							 "Container predicate·adjective)");

		auto clause= new LinkingClause 
							(	new Container // subject
									([	new Noun (Noun.Type.Common, "milk")
									]),
								[	// verb_items
									new Verb (Verb.Type.Linking, "tastes")    // LinkingVerb
								],
								new Container
								([	// predicate·noun
									new Adjective ("delicious")
								])
							);

		assert (clause.language_type == Language.Type.Container);
		assert (clause.toString      == "milk tastes delicious");
		assert (clause.otag          == "<linkingclause>");
		assert (clause.ctag          == "</linkingclause>");
	}
}

class Word :Syntax
{
	//this(Args...)(auto ref Args args) { super(args); }

	this (in string text, in Syntax.Type type= Syntax.Type.none) 
	{
		super (type, text);
	}

	unittest
	{
		stdout.writeln("Word class unittest");

		auto word= new Word("verb", Syntax.Type.Verb);
		assert (word.type        == Syntax.Type.Verb);
		assert (word.language_type == Language.Type.Item);
		assert (word.text          == "verb");
		assert (word.toString      == "verb");
		assert (word.otag          == "<word>");
		assert (word.ctag          == "</word>");
	}
}


*/

/*class ClauseSubject :Container
{
	this (in Syntax item) 
	{	
		super (item);
	}

	this (in Syntax[] items) 
	{	
		super (items);
	}

	override
	string tagname () const
	{
		return "subject";
	}

	// simple·subject i
	unittest
	{
		stdout.writeln("ClauseSubject class test1 (simple·subject: i)");

		auto subject= new ClauseSubject ( new Pronoun (Pronoun.Type.Personal, "i"));
		assert (subject.language_type == Language.Type.Container);
		assert (subject.text          is null);
		assert (subject.toString      == "i");
		assert (subject.otag          == "<subject>");
		assert (subject.ctag          == "</subject>");

		auto item= subject.items[0];     // Syntax (Pronoun)
		assert (         item.text == "i");
		assert (     item.toString == "i");
		assert (item.language_type == Language.Type.Syntax);
		assert (item.otag          == "<pronoun>");
		assert (item.ctag          == "</pronoun>");

		auto pronoun= cast(Pronoun)item;
		assert (pronoun.syntax_type == Syntax.Type.Pronoun);
	}


	// compound·subject Alice and Lewis
	unittest
	{
		stdout.writeln("Subject class test 2 (compound: Alice and Lewis)");

		auto subject= new ClauseSubject
							([	new                    Noun (Noun.Type.Proper, "Alice"),
								new CoordinatingConjunction (CoordinatingConjunction.Type.AND),
								new                    Noun (Noun.Type.Proper, "Lewis")
							]);

		assert (subject.language_type == Language.Type.Container);
		assert (subject.text          is null);
		assert (subject.toString      == "Alice and Lewis");
		assert (subject.otag          == "<subject>");
		assert (subject.ctag          == "</subject>");

		auto item0= subject.items[0];     // Syntax (Noun)
		assert (         item0.text == "Alice");
		assert (     item0.toString == "Alice");
		assert (item0.language_type == Language.Type.Syntax);
		assert (item0.otag          == "<noun>");
		assert (item0.ctag          == "</noun>");

		auto noun= cast(Noun)item0;
		assert (   noun.syntax_type == Syntax.Type.Noun);
		assert (noun.noun_type == Noun.Type.Proper);
	}

	unittest
	{
		stdout.writeln("Subject class test 2 (adjective·subject)");

		auto subject= new ClauseSubject
							([	new Determiner (Determiner.Type.Definite),
								new CommonNoun ("woman"),
									// adjective·clause
								new RelativePronoun ("who"),  // relative·pronoun
								new      ActiveVerb ("looked"),
								new       Adjective ("happy")
							]);

		assert (subject.language_type == Language.Type.Container);
		assert (subject.text          is null);
		assert (subject.toString      == "the woman who looked happy");
		assert (subject.otag          == "<subject>");
		assert (subject.ctag          == "</subject>");
	}
}

class ClauseActivity :Container 
{	// content:
	// · verb·phrase : helping·verb + action·verb or helping·verb + linking·verb
	// · phrasal·verb: verb + preposition, adverb, both

	this (in Syntax[] items)
	{
		super (items);
	}

	this (in Verb item)
	{
		super (item);
	}

	override
	string tagname () const
	{
		return "activity";
	}
}

class ClauseObject :Container
{	// contents:
	// · Noun, Pronoun, Adjective, Determiner, Conjunction, Preposition ...

	this (in Syntax item) 
	{	
		super (item);
	}

	this (in Syntax[] items) 
	{	
		super (items);
	}

	override
	string tagname () const
	{
		return "object";
	}

	
	// compound·subject Alice and Lewis
	unittest
	{
		stdout.writeln("ClauseObject class test (the backyard)");

		auto object= new ClauseObject
							(
								([	new Determiner (Determiner.Type.Definite),
									new Noun       (Noun.Type.Common, "backyard")
								])
							);

		assert (object.language_type == Language.Type.Container);
		assert (object.toString      == "the backyard");
		assert (object.otag          == "<object>");
		assert (object.ctag          == "</object>");

		auto item0= object.items[0];     // Syntax (Determiner)
		assert (item0.language_type == Language.Type.Syntax);
		assert (         item0.text == "the");
		assert (     item0.toString == "the");
		assert (item0.otag          == "<determiner>");
		assert (item0.ctag          == "</determiner>");
	}
}
*/

